angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $location, $state, $ionicPopup, $ionicLoading, $http) {

    $scope.loginData = {};

    $scope.doLogin = function() {
    
    $ionicLoading.show( {
    template: 'Signing in...',
    noBackdrop : true,
        });

    var username = $scope.loginData.usrName;
    var usrPass = $scope.loginData.usrPass;

    $http.get("https://localhost/houseStay/api/login.php?usrName="+username+"&pswd="+usrPass).
    success(function(data) {

    localStorage.setItem("profileData",JSON.stringify(data));

    var loginData = data;

      if (loginData[0] != undefined)
      {
              $scope.$root.stat1 = false;
              $scope.$root.stat2 = true;
    localStorage.setItem("id", data[0].id);
    localStorage.setItem("uid", data[0].id);
    localStorage.setItem("urlProfile", data[0].url);
              $state.go('app.profile');
      }
        
      else
      {
           $ionicPopup.alert({
           title: 'Error',
           template: 'Incorrect username or password'
         });
      }  

    $ionicLoading.hide();
    })

    .error(function(data) {
          $ionicLoading.hide();
           $ionicPopup.alert({
           title: 'Error',
           template: 'Network failed'
         });
    });

    }

  $scope.doRegister = function() {
   $location.path('/registerTab/register');
  };

  $scope.doGuest = function() {
              $scope.$root.stat1 = true;
              $scope.$root.stat2 = false;
        $state.go('app.main');
  };


})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('RegisterCtrl', function($scope, $ionicPlatform, $location, $state, $http, $ionicLoading, $ionicPopup, $cordovaCamera, $cordovaFile) {
 
  $scope.registerData = {};

  $scope.doRegister = function()
  {
        $ionicLoading.show( {
    template: 'Signing in...',
    noBackdrop : true,
        });

    var name = $scope.registerData.name;
    var userName = $scope.registerData.userName;
    var password = $scope.registerData.password1;
    var website = $scope.registerData.website;
    var bio = $scope.registerData.bio;   
        var url = $scope.registerData.url;  

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/register.php?name="+name+"&userName="+userName+"&password="+password+"&website="+website+"&bio="+bio+"&url="+url).
    success(function(data) {

    $ionicPopup.alert({
     title: 'Success',
     template: 'Please Login'
   })

    .then(function(res) {
    $location.path('/loginTab/login');
   });

    $ionicLoading.hide();
    })

        .error(function(data) {
          $ionicLoading.hide();
           $ionicPopup.alert({
           title: 'Error',
           template: 'Network failed'
         });
    });

 }




  $scope.goBack = function()
  {
    $location.path('/loginTab/login');
  }

})

.controller('ProfileCtrl', function($scope, $location, $http, $state, $ionicPopup) {

   var id = localStorage.getItem("id");
    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/profile.php?id="+id).
    success(function(data) {
      
      $scope.data = data;

    });

 // window.localStorage.setItem("uid", $scope.profileData[0].id);


    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/allHouseUser.php?uid="+id).
    success(function(data) {
      
      $scope.house = data;

    });
  $scope.goEdit = function()
  {
   $location.path('/updateTab/update');
  }

  $scope.goDelete = function(id)
  {
    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/delete.php?id="+id).
    success(function(data) {
      
      var myPopup = $ionicPopup.show({
           title: 'Success',
           template: 'Housestay Deleted',
            buttons: [
       { text: 'Ok' }],
        type: 'button-positive'
     })
         
        .then(function(res) {

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/allHouseUser.php?uid="+$scope.id).
    success(function(data) {
      
      $scope.house = data;

    });

   });


    });

    $state.reload();
  }
})

.controller('ManageCtrl', function($scope, $location, $http, $ionicPopup) {

  var uid = localStorage.getItem("uid");
    
    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/allHouseUser.php?uid="+uid).
    success(function(data) {
      console.log(data);
      $scope.house = data;
    localStorage.setItem("allHouseUser",JSON.stringify(data));

          });

  $scope.goEdit = function()
  {
    console.log("edit");
  }

  $scope.goUpdate = function(hid)
  {
     $location.path('/app/updateHouse');
     window.localStorage.setItem("hid", hid);
  }

})

.controller('UpdateCtrl', function($scope, $location, $ionicLoading, $http, $ionicPopup) {

  $scope.updateData = {};

$scope.urlProfile = localStorage.getItem("urlProfile");

  $scope.doUpdate = function()
  {
        $ionicLoading.show( {
    template: 'Updating...',
    noBackdrop : true,
        });

    var name = $scope.updateData.name;
    var userName = $scope.updateData.username;
    var password = $scope.updateData.password;
    var website = $scope.updateData.website;
    var bio = $scope.updateData.bio;   
        var url = $scope.updateData.url;  
          var uid = localStorage.getItem("uid");

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/updateUser.php?name="+name+"&userName="+userName+"&password="+password+"&website="+website+"&bio="+bio+"&url="+url+"&uid="+uid).
    success(function(data) {

    $ionicPopup.alert({
     title: 'Success',
     template: 'Info Updated'
   })

    .then(function(res) {
    $location.path('/app/profile');
   });

    $ionicLoading.hide();
    })

        .error(function(data) {
          $ionicLoading.hide();
           $ionicPopup.alert({
           title: 'Error',
           template: 'Network failed'
         });
    });
  $scope.goBack = function()
  {
    $location.path('/app/profile');
  }
}

})

.controller('RegisterHouseCtrl', function($scope, $location, $http, $ionicPlatform, $ionicPopup) {

    $ionicPlatform.ready(function() {
    //you app code goes here  
          document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
    console.log(navigator.camera);
}
});

  $scope.houseData = {};

  $scope.doRegister = function()
  {

  var lat = $scope.houseData.lat;
  var lang = $scope.houseData.lang;
  var location = $scope.houseData.location;
  var name = $scope.houseData.name;
  var phone = $scope.houseData.phone;
  var address = $scope.houseData.address;
  var facilities = $scope.houseData.facilities;
  var price = $scope.houseData.price;
  var url = $scope.houseData.url;
  var uid = localStorage.getItem("uid");

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/registerHouse.php?location="+location+"&name="+name+"&phone="+phone+"&address="+address+"&facilities="+facilities+"&price="+price+"&uid="+uid+"&lat="+lat+"&lang="+lang+"&url="+url).
    success(function(data) {
      
      console.log(data);
           $ionicPopup.alert({
           title: 'Success',
           template: 'Housestay Registered'
         });
    });


}
  $scope.goBack = function()
  {
    $location.path('/app/registerHouse');
  }

     $scope.openPhotoLibrary = function() {

 var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 1024,
        targetHeight: 768,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };
 
      uploadPicture(options);

 function uploadPicture(options) {
      $cordovaCamera.getPicture(options).then(function (sourcePath) {
        var sourceDirectory = sourcePath.substring(0, sourcePath.lastIndexOf('/') + 1);
        var sourceFileName = sourcePath.substring(sourcePath.lastIndexOf('/') + 1, sourcePath.length);
        sourceFileName = sourceFileName.split('?')[0];
      }, function (err) {
        console.log(err);
      });
    };

    }


})

.controller('MainCtrl', function($scope, $state) {
$scope.goMap = function()
{
  $state.go('app.map');
}

})

.controller('MapCtrl', function($scope, $ionicLoading, $compile) {
  $scope.initialize = function() {
    var myLatlng = new google.maps.LatLng(3.767531, 100.982445);
    
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  

})

.controller('SabakCtrl', function($scope, $location, $http, $state) {

  var location = "Pekan Sabak";

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserPlace.php").
    success(function(data) {
      
      $scope.data = data;

      console.log($scope.data);
    });

      $scope.items = [{
      id   : '101',
      name: 'HOUSETAY TERATAK UMMI ',
      address  : 'TERATAK UMMI A 118 KG BANTING 45200 SABAK BERNAM SELANGOR',
      phone : '019-2846550 (PUAN NORSHIDAH BINTI SAIAN)',
      price : 'RM80 RINGGIT SATU MALAM',
      facilities : 'KEMUDAHAN: TV, ASTRO, AIR-COND, WATER HEATER, KEMUDAHAN TAMBAHAN: BANTAL, TILAM, KEMUDAHAN BERDEKATAN: SURAU, KEDAI RUNCIT, KEDAI MAKAN',
      others : 'DUA BUAH, 4 ORANG SATU RUMAH, 1 KELUARGA  JUGA  BOLEH MUAT DALAM SEBUAH RUMAH'
  },{
      id   : '102',
      name: 'HOUSETAY CLASSIC, KG BANTING',
      address  : 'KG. BANTING',
      phone : '019-2558880 (HAJI DASMIM BIN MORSAD)',
      price : 'SATU MALAM, RM150 RINGGIT',
      facilities : 'KATIL,TV, IRON BOARD, PETI SEJUK, AIR-COND, SOFA, MEJA MAKAN, SOFA, PINGGAN MANGKUK, TEMPAT LETAK SEJADAH, KEMUDAHAN BERDEKATAN: SURAU, KEDAI RUNCIT, KEDAI MAKAN',
      others : ': DUA BUAH, TIADA LIMIT ORANG NAK DUDUK'
  },
  {
      id   : '103',
      name: 'HOUSETAY D’RIMBUN, SEKENDI',
      address  : 'LOT 259, SIMPANG 3, TEBUK HAJI NOOR, JALAN BESAR KAMPUNG',
      phone : '019-3970650(HAMIDAH) / 019-6180144(MD WANAZIR',
      price : 'RM 90 ADA AIR-COND & KIPAS, RM 70 AIR-COND & KIPAS, RM 60 KIPAS, KIPAS SHJ',
      facilities : 'WATER HEATER, KATIL, AIR-COND, KIPAS, KEMUDAHAN TAMBAHAN: DAPUR, TILAM, BANTAL, BASIKAL, PA SYSTEM, KEMUDAHAN BERDEKATAN: SURAU, KEDAI RUNCIT, KEDAI MAKAN, BENGKEL, KERETA',
      others : 'LIMA BUAH, 5-6 ORANG DALAM SATU RUMAH'
  },
  {
      id   : '104',
      name: 'HOUSESTAY JAGUH',
      address  : 'KG. TELUK BELANGA, PARIT BARU, SABAK BERNAM, SELANGOR',
      phone : '017-2110796(FAREKHA) / 019-3778881(SAHLAN)',
      price : 'RM80 (1 BILIK)/ RM100 (2 BILIK)',
      facilities : 'TV, PETI AIS, IRON,AIRCOND,KIPAS,WATER HEATER, KATIL 1 SINGLE 1 QUEEN, KEMUDAHAN BERDEKATAN: KEDAI MAKAN, PADANG FUTSAL, SURAU',
      others : '9 RUMAH'
  },
  {
      id   : '105',
      name: 'TERATAK BONDA',
      address  : 'JALAN PEKAN, KG. TEBUK SERI TANJUNG, 45400, SUNGAI AYER TAWAR, SELANGOR',
      phone : '019-4668807(SOHIR)/017-6735898(HAJAR)',
      price : 'RM150/RM180',
      facilities : 'AIRCOND, KIPAS, TV, ASTRO, DAPUR, PETI AIS, KEMUDAHAN BERDEKATAN: KEDAI MAKAN, SURAU',
      others : 'DUA BUAH, 2 BILIK'
  },
  {
      id   : '106',
      name: 'HOUSESTAY WAK JENAL',
      address  : 'NO 1, JALAN KEBUN BARU TEBUK JAWA, 45200, SABAK BERNAM',
      phone : '016-3714645',
      price : '',
      facilities : 'PETI AIS, TV, ASTRO, SOFA, DAPUR, AIRCOND, KEMUDAHAN TAMBAHAN: BANTAL, TOTO',
      others : 'SATU BUAH, 2 BILIK'
  },
  {
      id   : '107',
      name: 'HOUSESTAY ABI',
      address  : 'LOT 3817, KG. BANTING',
      phone : '017-5723558',
      price : 'RM75',
      facilities : 'KATIL 1 QUEEN 1 DOUBLE DECKER,TV, AIRCOND, DAPUR(ASING)',
      others : '4 RUMAH'
  }];


   localStorage.setItem("sabakHouse",JSON.stringify($scope.items ));

    $scope.goDetails = function(id)
    {
      window.localStorage.setItem("houseDetailsID", id);
      $state.go('app.houseDetails');
    }

   
})

.controller('ManisCtrl', function($scope, $location, $http) {

    var location = "Air Manis";

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserPlace.php").
    success(function(data) {
      
      $scope.data = data;
    });

        $scope.goDetails = function(id)
    {
      window.localStorage.setItem("houseDetailsID", id);
      $state.go('app.houseDetails');
    }

})

.controller('TawarCtrl', function($scope, $location, $http) {

  var location = "Sungai Air Tawar";

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserPlace.php").
    success(function(data) {
      
      $scope.data = data;
    });

        $scope.goDetails = function(id)
    {
      window.localStorage.setItem("houseDetailsID", id);
      $state.go('app.houseDetails');
    }
})

.controller('FacilitiesCtrl', function($scope, $state) {

      $scope.items = [{
      id   : '1',
      name : 'Cabin Inc Studio',
      owner: 'Md Hairuddin Margilan',
      phone  : '018-6694030',
      address : 'Jalan Pekan Parit Baru, 45100 Sungai Ayer Tawar)',
      service : '1 Hour Jamming (RM 25), Instrument and PA system Rental',
  },
  {
      id   : '2',
      name : 'Futsal Wak Salim',
      owner: 'Mohd Salim B Jarkasi',
      phone  : '013-3436851',
      address : 'Jalan Masjid, Kampung Parit Baru, 45100 Sungai Ayer Tawar',
      service : '1 Hour (RM 20)',
  },
    {
      id   : '3',
      name : 'Klinik Kesihatan Parit Baru',
      owner: '',
      phone  : '',
      address : 'Jalan Masjid, Kampung Parit Baru, 45100 Sabak',
      service : 'Bilik Rawatan dan Kecemasan, Makmal dan Farmasi, Jabatan Pesakit Luar, Klinik Pergigian',
  },
  {
      id   : '4',
      name : 'Kolam Pancing Pembela',
      owner: '',
      phone  : '',
      address : 'No.25 , Jalan Bagan Beting Kepah, 45100 Sungai Air Tawar, Selangor',
      service : 'Ikan Jenahak, Naga Harimau, Siakap, Kaci, Bawal, Kerapu',
  },
  {
      id   : '5',
      name : 'Masjid Jame Mahmudiah',
      owner: '',
      phone  : '',
      address : 'Jalan Masjid, Jalan Haji Mahmud , 45100 Sungai Ayer Tawar',
      service : '',
  },
  {
      id   : '6',
      name : 'Masjid Jamek Sultan Hisamuddin',
      owner: '',
      phone  : '',
      address : ': Jalan Kampung Batu Tiga Buluh Tujuh, Sabak Bernam, Selangor',
      service : '',
  },
  {
      id   : '7',
      name : 'Pasar Mini',
      owner: 'Latif Helmi B Mohd Balwi',
      phone  : '019-6278971 ',
      address : '49 Jalan Kg. Banting, 45200 Sabak Bernam',
      service : '',
  },
  {
      id   : '8',
      name : 'Pekan Parit Baru',
      owner: '',
      phone  : '',
      address : 'Kampung Tebok Seri Tanjung ,45100 Sungai Air Tawar',
      service : 'Agent license, restaurant, market, agro bazaar pasar tani, stage and playground',
  },
  {
      id   : '9',
      name : 'Pekan Tanah Lesen',
      owner: '',
      phone  : '',
      address : 'Jalan Tangki Air, Tanah Lesen, 45100 Sungai Ayer Tawar',
      service : 'Cyber cafe, restaurant, market, agro bazaar pasar tani, stage and playground',
  },
  {
      id   : '10',
      name : 'Putera Khai Barber Shop',
      owner: 'Muhammad Khairifdi Bin Ghazali',
      phone  : '011-23371993',
      address : 'Jalan Bagan Nakhoda Omar, Kampung Parit Baru',
      service : 'Haircut Regular, Haircut and Style, Haircut Child, Hot Shave, Hair Treatment, Wash and Blow, Rebonding, Pomade',
  },

  ];

      localStorage.setItem("facData",JSON.stringify($scope.items));

  $scope.goFacilities = function(id){
      window.localStorage.setItem("facID", id);
    $state.go('app.facDetails') 
  }

})

.controller('FacDetailsCtrl', function($scope, $ionicSlideBoxDelegate,  $ionicLoading, $compile) {
    
     $scope.items =  JSON.parse(localStorage.getItem('facData'));

  var facID = localStorage.getItem("facID");

    $scope.initialize = function() {

      if (facID == 1)
      {
       var myLatlng = new google.maps.LatLng(3.8212,100.8387);     
      }
      else if (facID == 2)
      {
       var myLatlng = new google.maps.LatLng(3.8174,100.8381); 
      }
      else if (facID == 3)
      {
       var myLatlng = new google.maps.LatLng(3.8169,100.8435); 

      }
      else if (facID == 4)
      {
       var myLatlng = new google.maps.LatLng(3.7491,100.9264); 
      }
      else if (facID == 5)
      {
       var myLatlng = new google.maps.LatLng(3.8165,100.8444); 

      }
      else if (facID == 6)
      {
       var myLatlng = new google.maps.LatLng(3.7708,100.9853); 

      }
      else if (facID == 7)
      {
       var myLatlng = new google.maps.LatLng(0,0); 

      }
      else if (facID == 8)
      {
       var myLatlng = new google.maps.LatLng(3.8199,3.8199); 

      }
      else if (facID == 9)
      {
       var myLatlng = new google.maps.LatLng(3.8062,100.8454); 

      }
      else if (facID == 10)
      {
       var myLatlng = new google.maps.LatLng(3.7951,100.8446); 

      }
    
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map2"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }

  if (facID == 1)
              {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f1_1.png",
            "img/f1_2.png"
         ]
      }
      else if (facID == 2)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f2_1.png",
            "img/f2_2.png"
         ]
       }
      }
      else if (facID == 3)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f3_1.png",
            "img/f3_2.png"
         ]
       }

      }
      else if (facID == 4)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f4_1.png",
            "img/f4_2.png"
         ]
       }
      }
      else if (facID == 5)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f5_1.png",
            "img/f5_2.png"
         ]
       }

      }
      else if (facID == 6)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f6_1.png",
            "img/f6_2.png"
         ]
       }
      }
      else if (facID == 7)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f7_1.png",
            "img/f7_2.png"
         ]
       }
      }
      else if (facID == 8)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f8_1.png",
            "img/f8_2.png"
         ]
       }
      }
      else if (facID == 9)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f9_1.png",
            "img/f9_2.png"
         ]
       }
      }
      else if (facID == 10)
      {
      {
                var facDetails = $scope.items;

                    angular.forEach(facDetails, function(value)
                    {
                        if (value.id == facID)
                        {
                            this.push(value);
                            $scope.currentFac = value;
                        }
                    }, facDetails);  

          $scope.data = $scope.currentFac;

              $scope.images = 
         [
          "img/f10_1.png",
            "img/f10_2.png"
         ]
       }
      } 

  $scope.slideVisible = function(index){
    if(  index < $ionicSlideBoxDelegate.currentIndex() -1 
       || index > $ionicSlideBoxDelegate.currentIndex() + 1){
      return false;
    }
    
    return true;
  }
})

.controller('MenuCtrl', function($scope, $state, $location) {

  $scope.doLogout = function() {
    window.localStorage.removeItem("profileData")
    $location.path('/loginTab/login');
  };

})

.controller('UpdateHouseCtrl', function($scope, $state, $location, $http) {

$scope.houseData = {};


$scope.doUpdate = function()
{

  var location = $scope.houseData.location;
  var name = $scope.houseData.name;
  var phone = $scope.houseData.phone;
  var address = $scope.houseData.address;
  var facilities = $scope.houseData.facilities;
  var price = $scope.houseData.price;
  var uid = localStorage.getItem("uid");

    $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/registerHouse.php?location="+location+"&name="+name+"&phone="+phone+"&address="+address+"&facilities="+facilities+"&price="+price+"&uid="+uid).
    success(function(data) {
      
      console.log(data);

    });

}

$scope.goBack = function()
{
   $state.go('app.manage');
}

})

.controller('HouseDetailsCtrl', function($scope, $state, $location, $ionicSlideBoxDelegate, $http, $ionicLoading) {


   $scope.items =  JSON.parse(localStorage.getItem('sabakHouse'));
  
  var id = localStorage.getItem("houseDetailsID");


$scope.doImg = function()
{
  $state.statP = true;
}

if (id == 101)
{
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;
      $scope.images = 
 [
  "img/h1_1.png",
    "img/h1_2.png",
      "img/h1_3.png"
 ];
// $scope.image1 = "img/h1_1.jpg";
// $scope.image2 = "img/h1_2.jpg";
// $scope.image3 = "img/h1_3.jpg";


}

else if (id == 102)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;

      $scope.images = 
 [
  "img/h2_1.png",
    "img/h2_2.png",
      "img/h2_3.png"
 ]

  }
}
else if (id == 103)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;

      $scope.images = 
 [
  "img/h3_1.png",
    "img/h3_2.png",
      "img/h3_3.png"
 ]

  }
}
else if (id == 104)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;

      $scope.images = 
 [
  "img/h4_1.png",
    "img/h4_2.png",
      "img/h4_3.png"
 ]

  }
}
else if (id == 105)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;

      $scope.images = 
 [
  "img/h5_1.png",
    "img/h5_2.png",
      "img/h5_3.png"
 ]

  }
}
else if (id == 106)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;

      $scope.images = 
 [
  "img/h6_1.png",
    "img/h6_2.png",
      "img/h6_3.png"
 ]

  }
}
else if (id == 107)
{
  {
    $scope.stat1 = false;
  $scope.stat2 = true;

    var houseDetails = $scope.items;

            angular.forEach(houseDetails, function(value)
            {
                if (value.id == id)
                {
                    this.push(value);
                    $scope.currentHouse = value;
                }
            }, houseDetails);  

  $scope.data = $scope.currentHouse;

      $scope.images = 
 [
  "img/h7_1.png",
    "img/h7_2.png",
      "img/h7_3.png"
 ]

  }
}
else
{
  $scope.stat1 = true;
  $scope.stat2 = false;

      $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserID.php?id="+id).
    success(function(data) {
      
      $scope.data = data;
     // console.log(data[0].url);

      window.localStorage.setItem("url", data[0].url)

          //http://i67.tinypic.com/23r1wu0.png

    });

$scope.images = 
 [
  localStorage.getItem("url"),
 ]

}

  $scope.slideVisible = function(index){
    if(  index < $ionicSlideBoxDelegate.currentIndex() -1 
       || index > $ionicSlideBoxDelegate.currentIndex() + 1){
      return false;
    }
    
    return true;
  }

     $scope.initialize = function() {

      //sdsdsdsd
    //var myLatlng = new google.maps.LatLng(3.767531, 100.982445);

      if (id == 101)
      {
        var myLatlng = new google.maps.LatLng(3.7504, 100.9236);
      }
      else if (id == 102)
      {
         var myLatlng = new google.maps.LatLng(3.7485, 100.9264);       
      }
      else if (id == 103)
      {
         var myLatlng = new google.maps.LatLng(3.7419, 100.9402);       
      }
      else if (id == 104)
      {
         var myLatlng = new google.maps.LatLng(3.8211 , 100.8274);       
      }
      else if (id == 105)
      {
         var myLatlng = new google.maps.LatLng(3.8209, 100.8327);       
      }
      else if (id == 106)
      {
         var myLatlng = new google.maps.LatLng(3.7575, 100.9012);       
      }
      else if (id == 107)
      {
         var myLatlng = new google.maps.LatLng(3.7531 , 100.9190);       
      }
      else
      {

      $http.get("https://syafiqevo.000webhostapp.com/houseStay/api/houseUserID.php?id="+id).
    success(function(data) {
      console.log("kor ", data);

      // $scope.lat = parseFloat(data[0].lat) ;
      // $scope.lang = parseFloat(data[0].lang) ;

      window.localStorage.setItem("lat", parseFloat(data[0].lat));
      window.localStorage.setItem("lang", parseFloat(data[0].lang));
     
      //var myLatlng = new google.maps.LatLng(lat , lang);       

      //$scope.hDetails = data;
    });
    var lat = localStorage.getItem("lat");
    var lang = localStorage.getItem("lang");


      var myLatlng = new google.maps.LatLng(lat , lang);       

      }


    //sdsdsd
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map3"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  
  // Called to navigate to the main app
  $scope.startApp = function() {
    $state.go('main');
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };
})


.controller('RateCtrl', function($scope, $state, $location) {


});




















