// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'MenuCtrl'
  })

    .state('loginTab', {
    url: '/loginTab',
    abstract: true,
  cache: false,
    templateUrl: 'templates/loginTab.html',
  controller: 'LoginCtrl'
  
  })
  
    .state('loginTab.login', {
    url: '/login',
  cache : false,
    views: {
      'login': {
        templateUrl: 'templates/login.html'
      }
    }
  })

  .state('registerTab', {
    url: '/registerTab',
    abstract: true,
    cache: false,
    templateUrl: 'templates/registerTab.html',
  })
  
  .state('registerTab.register', {
    url: '/register',
  cache : false,
    views: {
      'register': {
        templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'
      }
    }
  })

  .state('updateTab', {
    url: '/updateTab',
    abstract: true,
    cache: false,
    templateUrl: 'templates/updateTab.html',
  })
  
  .state('updateTab.update', {
    url: '/update',
  cache : false,
    views: {
      'update': {
        templateUrl: 'templates/update.html',
        controller: 'UpdateCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })

    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.manage', {
    url: '/manage',
    views: {
      'menuContent': {
        templateUrl: 'templates/manage.html',
        controller: 'ManageCtrl'
      }
    }
  })

 .state('app.registerHouse', {
    url: '/registerHouse',
    views: {
      'menuContent': {
        templateUrl: 'templates/registerHouse.html',
        controller: 'RegisterHouseCtrl'
      }
    }
  })

.state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainCtrl'
      }
    }
  })

.state('app.map', {
    url: '/map',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  })

.state('app.sabak', {
    url: '/sabak',
    views: {
      'menuContent': {
        templateUrl: 'templates/sabak.html',
        controller: 'SabakCtrl'
      }
    }
  })

.state('app.manis', {
    url: '/manis',
    views: {
      'menuContent': {
        templateUrl: 'templates/manis.html',
        controller: 'ManisCtrl'
      }
    }
  })

.state('app.tawar', {
    url: '/tawar',
    views: {
      'menuContent': {
        templateUrl: 'templates/tawar.html',
        controller: 'TawarCtrl'
      }
    }
  })

.state('app.facilities', {
    url: '/facilities',
    views: {
      'menuContent': {
        templateUrl: 'templates/facilities.html',
        controller: 'FacilitiesCtrl'
      }
    }
  })

.state('app.facDetails', {
    url: '/facDetails',
    views: {
      'menuContent': {
        templateUrl: 'templates/facDetails.html',
        controller: 'FacDetailsCtrl'
      }
    }
  })

.state('app.updateHouse', {
    url: '/updateHouse',
    views: {
      'menuContent': {
        templateUrl: 'templates/updateHouse.html',
        controller: 'UpdateHouseCtrl'
      }
    }
  })

.state('app.houseDetails', {
    url: '/houseDetails',
    views: {
      'menuContent': {
        templateUrl: 'templates/houseDetails.html',
        controller: 'HouseDetailsCtrl'
      }
    }
  })

.state('app.rate', {
    url: '/rate',
    views: {
      'menuContent': {
        templateUrl: 'templates/rate.html',
        controller: 'RateCtrl'
      }
    }
  });









  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/loginTab/login');
});
